# JSON

> language déclaratif (yaml, html, css, toml, xml)

JavaScript Object Notation

On y trouve :

```json
[Array]
{Object}
"string"
true
Number (integer, float);

"key" : "value"
```

le json c'est du text

Utilisé pour le dialogue client-serveur (ajax) asynchronous javascript, envoie des données depuis le serveur, et uniquement des données, on balance un fichier js et on dialogue pour avoir juste la data, on a l'illusion de changer de page.

stateless

On l'utilise quand on a besoin de donnée statique (const), qui ne bougent pas.

parser = interpreter

cookie = fichier text destiné à l'us, : espace de stockage sécurisé, chose sensible à stocker.

Autres zone de stockage = local, pas sécurisé. quand on veut un truc persistant.

La session storage se supprime à la fin de la session, pas sécurisé.

## Exemple

```json
{
  "promo": 6,
  "city": "Bout-du-Monde",
  "Members": [
    {"name": "Henri", "surname": "Patate"},
    {"name": "Jack", "surname": "Carotte"}
  ],
  "open": false
}
```