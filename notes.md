# Notes

```terminal
git init
npm init -y
npm install webpack webpack-cli --save-dev

code.
```

## webpack.config.js

```js
module.exports = {
  mode: 'development',
  entry: './src/jt/index.js',
  devtool: 'inline-source-map'
};
```

## .gitignore

```.gitignore
node_modules
```

```terminal
git remote add origin monAddGit
```

## package.json

> Dans le script :

```json
"dev": "webpack --watch"
```

```terminal
mkdir src
cd src
touch index.html
mkdir js
cd js
touch index.js
```

## Html

```html
<script src="../dist/main.js"></script>
```

```terminal
commit
push
```

* [Exemple](https://gitlab.com/simplonlyon/P6/heritage)