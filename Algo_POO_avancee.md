## Algo et POO avancée

### Les bases

- Encapsulation
Regrouper les éléments semblables, qui ont des éléments communs (classes)

- Héritage
Permet de spécialiser une classe (parent - enfant)

- Polymorphisme
Redéfinir / Préciser une méthode d'un parent (supercharge / surcharger)

- Classe et instance
La classe et le schéma permetant la création d'instances

- Visibilité
Publique, protégé, privé (public, protected, private)

### Statique
- Les propriétés et méthodes sont dites statiques lorsqu'elles sont inaccessibles en dehors de toutes instances d'une classe
- Utilisé entre autrres dans le design pattern _Singleton_
Exemple:
```php
class DB {
    static protected $connexion;
    private function __construct(){}
    static function getInstance(){
        self::$connexion = new DB();
        return self::$connexion;
    }
}
//Le constructeur étant privé, on utilise la méthode static pour créer l'instance
$db = DB::getInstance();
```
- Pas spécifique POO: une variable dans une fonction peut être statique

## Abstraction
- Une classe abstraite ne peut pas être instanciée, il faut en **hériter** pour l'utiliser
- Une méthode abstraite ne peut pas être appelée, ont doit la surcharger
Chaques méthodes abstraites doivent être redéfinies pour chaque enfants

## Interface
- Une interface est un ensemble de signature de péthodes publiques
- Une classe peut implémenter plusieurs interface
- Une classe qui implémente une interface doit implémenter toutes ses méthodes, en respectant leurs signatures (le contrat)
- Utilisé dans les design pattern _Strategy_ et  _Inversion of Control_

## Trait
- Un trait permet de partager du code entre plusieurs classes
- Couplé avec une interface, il permet d'implémenter l'héritage multiple dans les langages n'en disposant pas

| Propriété   | Type   |
| ----------- | ------ |
| Name        | string |
| Pseudo      | string |
| URL         | string |
| Password    | string |
| Description | string |
| Tag         | string |

