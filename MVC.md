# MVC

[Wikipédia](https://fr.wikipedia.org/wiki/Mod%C3%A8le-vue-contr%C3%B4leur)

* **M**odèle (données)
* **V**ue (affichage)
* **C**ontrôleur (lien vue et contrôleur)

***

Pratique si on a différentes vues pour un même modèle ou encore pour faire un import/export

> Templating ?

**Design pattern** = patron de conception

architecture d'application

Le MVC c'est un peu comme un oignon, il y a des couches. Elles sont de moins en moins abstraites à mesure.

Le model dans le front end s'occupe principalement de dialoguer avec le serveur

***

MVW (Whatever)