# AJAX

[TOC]

> Faire des requête HTTP en JS.

Le JS par défaut n'a accès uniquement qu'au document HTML. Pour manipuler des données lier à une bdd on va utiliser AJAX.

Tout les serveurs n'autorisent pas le requête JS, il faut parametrer le serveur pour lui préparer le le terrain. Il faut aussi préparer coté client toutes les requête pour amener vers les routes.

* GET, PUT, POST, DELETE

![ajax](./images/ajax.png)

```js
import $ from 'jquery';

$('button').on('click', function(){
  $.ajax('http://localhost:3000/package.json')
    .then(function(response){
      $('p').text(response.name);
    });
});
```

`Payload` : charge utile

Principes :

1. On utilise http pour renvoyé des données brutes
2. Deserialiser
3. Utiliser les données

Modèle synchrone : serveur et client même état.

Le Js fait la passerelle entre les informations et la vue.

***

On envoie des données brutes

***

Un framework c'est une façon de faire, une librairie est une boite à outil.

Modèle asynchrone : Le code ne s'execute plus de manière sequentielle, l'état du serveur et du client n'est par le même. On peut avoir un seul backend pour plusieurs frontend ! Multisupport ! On ne gère que la donnée, pas de visualisation, ça permet de réduire la charge du serveur, c'est le principe d'être stateless, sans état, on est donc asynchrone.

La convention dans les URI : ressource, id ou action, ou autre ressource

On utilise rest pour ça, protocole (dp, ...) http

***

Grâce à Rest on peut être parfaitement stateless.

```js
$ajax('url ressource')
    .then(function()){} // vient d'une classe qui s'appelle : Promise
```

## Promise

Une **promise** c'est avant tout un "**event**" qui se déclenche quand on a receptionné les données.

* Promesse de données / du contenu
* then/catch

```js
Promise <string> // On essaye de typer les promesses que l'on reçoit
```

## Le chaînage des `then`

```js
let promise = new Promise(function(resolve, reject){
  resolve("bloup");
});

promise.then(function(data){
    return data.toUpperCase();
}).then(function(upData){
    console.log(upData);
});
```

```js
function asyncAdd(){
  let promise = new Promise(function(resolve, reject){
    resolve("bloup");
  });
  return promise;
}

asyncAdd().then(function(data){
  console.log(data);  
});
```

```js
function asyncAdd(a, b){
  let promise = new Promise(function(resolve, reject){

    setTimeout(function(){
      resolve(a+b);
    }, 1000);
  });
  return promise;
}

asyncAdd(2, 2).then(function(data){
  console.log(data);  
});

console.log("bloup");
```

```js
function asyncAdd(a, b){
  let promise = new Promise(function(resolve, reject){

    if (typeof(a) !== 'number' || typeof(b) !== 'number') {
      reject("paramaters must be numbers");
    }

    setTimeout(function(){
      resolve(a+b);
    }, 1000);
  });
  return promise;
}

asyncAdd("toto", 2).then(function(data){
  console.log(data);  
}).catch(function(err){
  console.log(err);
});

console.log("bloup");
```

> MVW (Model View Whatever)

![ajax](./images/ajax-rest-client.png)