# Test unitaire

```shell
sudo apt install php7.2-zip php7.2-mbstring
```

Un test unitaire sert à prendre en compte tout les scenario possible d'une méthode, pour voir comment elle va se comporter.

Un cahier des charges et si il y a un problème, la méthode n'est pas valide.

C'est un test qui permet de tester un bout de code isolé (une unité) dans un environnement contrôlé et reproductible.

Un [mock](https://fr.wikipedia.org/wiki/Mock) (simulacres ou mock object), est une fausse version d'une méthode qui renvoie exactement ce qu'on attend.

```shell
./bin/phpunit
```

```php
<?php

namespace App\Tests\Util;

use PHPUnit\Framework\TestCase;
use App\Util\Greeter;

class GreeterTest extends TestCase {

  public function testFirst(){
    $this->assertEquals("bloup","bloup");
    $this->assertFalse(false);
  }

  public function testGreetSuccess()
  {
    $greeter = new Greeter();
    $result = $greeter->greet("bloup");
    $expect = "Hello bloup, how's it going ?";

    $this->assertEquals($expect, $result);
  }
}
?>
```

***

## /!\ MàJ pas exécuté /!\

```shell
sudo /usr/local/bin/composer self-update
sudo chmod 755 /usr/local/bin/composer
sudo chown $USER:$USER ~/.composer -R
composer update --no-plugins
sudo composer self-update --rollback
```

***

## CI/CD [!w.en](https://en.wikipedia.org/wiki/CI/CD)

* [Cucumber](https://cucumber.io/)

![CICD](./images/CICD.png)

***

* Test d'intégration
* Test fonctionnel

***

* [Nightwatchjs](http://nightwatchjs.org/)
* [Selenium](https://docs.seleniumhq.org/projects/ide/)

***

## V2 :arrow_right: Angular

Framework : Jasmine

> karma.conf.js

* [Mock](https://fr.wikipedia.org/wiki/Mock_(programmation_orient%C3%A9e_objet))