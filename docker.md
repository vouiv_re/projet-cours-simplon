# Docker

> Orchestration de container

Docker est composé de 3 partie :

* **docker engine** s'occupe des containeurs : basé sur lx.
* **docker compose** défini l’interaction entre différent docker.
* **docker swarm** est spécialiste du cloud, il reparti l'application sur plusieurs machine physique.

Les 2 premiers (engine et compose) vont nous intéresser.

## Terminal

* **Ctrl-C** : permet d'interrompre le processus.

```terminal
docker-compose up
```

```terminal
docker kill $(docker ps -q)
```

Tue le processus en cours (le container docker).

## Configuration

```yml
version: "3"
services:
  nginx:
   image: library/nginx:latest
   volumes:
    - ".:/usr/share/nginx/html"
   ports:
    - "80:80"
   networks:
    - nginx
networks:
  nginx:

volumes:
  socket:
```

> Exemple de fichier de configuration : docker-compose.yml

```undefined
FROM library/ubuntu:latest

CMD "apt update -y"
```

> Exemple de fichier : Dockerfile

***

## Engine

La difference entre deux système d'exploitation ce sont les fichiers qui les composent.

Un docker c'est un système de couche :
| App   |
| :---: |
| Nginx |
| Ubuntu serveur |
| Ubuntu 18.04   |

C'est le principe de layers, LXC, ça c'est développé par linux, docker ajoute une couche.

> [Kubernetes](https://kubernetes.io/) (google), est libre, remplace swarm et compose.

***

## Système de fichier

* NTFS = Windows
* Fat32 (-4go)
* Ext4 = Stockage linux
* SWAP = Swap Linux

[Wikipédia](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_fichiers)

> Couche d'abstraction de docker.

***

Une image c'est un plan pour construire un container.

***

```terminal
sudo usermod -a -G docker $USER
```

```terminal
sudo doker rmi monImage
```

```terminal
sudo docker create monImage
```

```terminal
sudo docker ps -a
```

```terminal
sudo docker start monConteneur
```

```terminal
sudo docker rm monConteneur
```

```terminal
man docker start
```

```terminal
top
htop
```

```terminal
ll /proc/
```

Marquer seulement le début de l'id du conteneur.

Pour lister les container.

Pour supprimer les images.

***

Le conteneur est toujours en root.

***

```docker
FROM library/ubuntu:latest
```

```docker
RUN
```

```docker
CMD
```

```terminal
sudo docker build .

sudo docker create 2f8de17f18d1

sudo docker ps -a

sudo docker start 777 && sudo docker ps
```

Un dockerfile permet de créer image qui permettent de créer un conteneur.

docker build -> docker create / docker images -> docker start / docker stop / docker ps / docker ps -a
dockerfile -> images -> container

***

un layers permet d'isoler entre les différents système.

cloisonnement = virtualisation ?

Virtualisation = emulation matérielle

***

1. download docker compose in /usr/local/bin/docker-compose

```terminal
sudo curl -L https://github.com/docker/compose/releases/download/1.17.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```

1. make docker-compose executable

```terminal
sudo chmod +x /usr/local/bin/docker-compose
```

1. Installing command completion

```terminal
sudo curl -L https://raw.githubusercontent.com/docker/compose/1.17.0/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
```

***

```terminal
docker-compose -v
```

docker compose on le configure avec du yaml.

```terminal
docker engine sudo docker-compose up
```

Pour démarrer

127.0.0.1 ou localhost

^C pour quitter

```terminal
sudo docker ps
```

## Nginx

nginx c'est dans le serveur, ça permet de servir d'intermédiaire pour rediriger les requêtes ?

c'est un reverse proxy.

C'est le serveur http qui permet de faire des requêtes (get post) vers une machine.

***

* fpm est un moteur php.
* nginx (russe) c'est très robuste
* apache c'est  aussi un serveur http, mais c'est caca.

***

Admin sys

Pas une machine virtuelle

scalimg (facebook) fédéere plusieurs machine pour faise tourner une app : cluster, grappe ?

Containerisation :warning: pas cloisonement ! pb de passer du mode dev au mode p°

Emulation > gaspillage energie (20-30% de la puissance) Virtual Private Server (VPS) parce que ça duplique tout, le materiel, l'os, processeur, ram, ...

Le but avoir le même comportement que le vistualisation à savoir le cloisonement

LXC Linux Container : on va creer une couche d'abstraction. On ne l'emule plus, on va utiliser ce que l'on a sous la main. on va déposer des couches logicels.

Docker n'est plus basé sur LXC.

Docker gaspi 1%

Doker à besoin du même noyau :warning: pour ses differents container.

***

load balancing :

* repartition rood robin  ?
* Tout mettre et attendre qui ce soit plein pour balancer sur une autre machine.

Combien d'instance veut on faire tourner ?

Outil de monitoring -> surveillance 

Prorata entre le poid de l'appli et la puissance des machines.

Cluster Managers

***

[Prometheus](https://prometheus.io/) : Power your metrics and alerting with a leading open-source monitoring solution.

[Graphana](https://hub.docker.com/r/grafana/grafana/) : Outil de visualisation.

***

ABtesting : 2 version diff du même site, les users sont dispatché aléatoirement, test et stat pour savoir ce qui plait et tester l'UX.

La tactique du salamis :1st_place_medal: Merci Guigui ! Modification d'un site petit à petit.

***

DevOps : Il fait le lien entre le sytème et le dev. Il a les connaissances dans les deux.

***

Effet de silo : Phénomène d'isolation des services (compétiton, répétition)

***

dockerfile

docker-compose.yaml

Un container c'est un seul process ! (php ou nginx ou ...)

![docker](images/docker.png)

> dockerfile

```dockerfile
FROM ubuntu

RUN mkdir toto
WORKDIR /toto

RUN echo "hello world" > test \
&& apt install zip

VOLUME ["/toto"]

CMD ["cat", "test"]
CMD ["ls"]
```

> Shell

```shell
docker build -t containerName .
docker run containerName
docker ps
```

Quand un container ne fait rien il s'allume et il s'eteint.

> docker-compote.yaml

```yaml
version: "3.1"
services:
	test:
		build:
			context: ./
			dockerfile: dockerfile
		volumes:
		- ./test:/toto
	toto:
		image: ubuntu
		links:
			- test
```

