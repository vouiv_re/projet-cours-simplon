# NPM

> Node Package Manager

npm est un gestionnaire (manager) de paquet node : Il va gérer les dépendance de nos projet.

node (nodejs?) c'est quand on place du js coté serveur (backend). Il permet de gérer la librairie et la version d'un projet.

## Semantic versioning

> V 1.0.0

L'écriture d'une version est composé de 3 parties :

1. Version majeure (major) : L'update majeure casse les fonctionnalités présentes dans les versions précédentes et ajoute une fonctionnalité majeure.
2. Version mineure (minor) : ajout de fonctionnalité
3. Patch : correction, ...

Le package.json nous renseigne sur toute les versions de nos paquets que le projet supporte. Le '**^**' nous dit qu’on a accès a tout jusqu’à la majeure d’après.

## Paquet

Un paquet peut être une librairie (sass?), un framework, outil (webpack?).

On peut lancer des script automatique (écrit dans le package.json), Un script peut en lancer un autre.

On peut lancer des tests unitaires ?

On va pouvoir installer des librairies via le node.

```terminal
npm run monScript
```

Raccourcis pour exécuter des commandes terminal définie dans le package.json.
> Une bonne pratique est d'expliquer dans le fichier README.md les run !

```json
"scripts": {
    "dev": "webpack --watch"
  },
```

```terminal
npm update minor
```

Fait une update de la version mineure de notre projet. Modifie dans le package.json la version du projet

> moment.js permet de gérer des horaires et des dates mieux que la méthode native.

```terminal
npm install moment --save
```

Permet d'installer la dernière version stable de la librairie + mettre à jour le package.json.

```terminal
npm init -y
```

Initialise mon repertoire avec un fichier package.json.

```terminal
npm install monPaquet --save-dev
```

Installe le paquet et l'ajoute dans les devDependencies.

```terminal
npm install
npm – i
```

Installe toutes les dépendances.

```terminal
npm install monPaquet --save
npm install monPaquet
```

Installe le paquet et l'ajoute dans les dépendances du fichier package.json.

```terminal
npm remove monPaquet
```

```terminal
npm -v
```

Renseigne la version de npm.

> cli = command line interface
***
> reg ex = expression régulière : regex.com

1 étape, 1 loader webpack.config ???

## Culture

> Npm aura de la concurrence : [yarn](https://yarnpkg.com/lang/en/) (pelote de laine).

## Liens utiles

* [Wikipédia](https://fr.wikipedia.org/wiki/Npm)

## Exemples

### package.json

```json
{
  "name": "nomProjet",
  "version": "1.0.0",
  "description": "",
  "main": "./src/js/main.js",
  "scripts": {
    "dev": "webpack --watch"
  },
  "repository": {
    "type": "git",
    "url": "git+ssh://git@gitlab.com/PEGeoffroy/projet-jeu2.git"
  },
  "author": "nomAuteur",
  "license": "ISC",
  "bugs": {
    "url": "https://monAdresseGit/issues"
  },
  "homepage": "https://monAdresseGit#README",
  "devDependencies": {
    "webpack": "^4.11.1",
    "webpack-cli": "^3.0.3"
  }
}
```

```json
{
  "name": "js-mvc",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "dev": "webpack --watch"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "css-loader": "^0.28.11",
    "node-sass": "^4.9.0",
    "sass-loader": "^7.0.3",
    "style-loader": "^0.21.0",
    "webpack": "^4.12.0",
    "webpack-cli": "^3.0.3"
  },
  "dependencies": {
    "bootstrap": "^4.1.1",
    "jquery": "^3.3.1",
    "popper.js": "^1.14.3"
  }
}
```