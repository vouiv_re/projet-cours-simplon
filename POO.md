# POO

Possibilité de créer des objets avec des propriétés.

Un objet est une représentation sous forme de code, d'un concept physique ou non, défini par des **propriétés** et des **méthodes**. Les propriétés sont les variables des objets.

Les objets peuvent interagir entre eux.

On utilise les objets pour organiser son code.

|  Stylo   |
| :------: |
|  taille  |
| couleur  |
|   type   |
| ecrire() |

```js
let firstObject = {
    propriete1: "valeur1",
    propriete2: true,
    secondObject: {
    	propriete3: "valeur3"
	},
    numb1: 1,
    method1: function(){
        console.log("Patate !");
    },
    method2(){
    	console.log("Carotte !");
    }
};

firstObject.propriete1 = "valeur2";

console.log(firstObject.secondObject.propriete3);
firstObject.method1();
```

Une classe est un moule avec lequel on créé des objets.

Un objet est une instance de classe.

```js
class MaClasse {
    //Constructeur - methode de l'objet class appelé avec new
  constructor(property1 = 'blue', property2 = 30, property3 = 'marqueur'){ 
    this.property1 = property1;
    this.property2 = property2;
    this.property3 = property3;
  }
    //Propriété
  write(text){
    let p = document.createElement('p');
    p.textContent = text;
    p.style.color = this.property1;
    p.style.fontSize = `${this.property2}px`;
  }
}
```

Une entité est une classe qui représente les données que l'on va stocker.

L'orienté objet n'optimise pas le code d'un point de vue performance.

Le JS est composé majoritairement d'objets, (objet natif ? objet primitif ?)

number - primitif / Number - objet

Pour acceder aux autres propriétés on utilise this.

Une classe est un shéma qui permet de creer des objets.

```js
{ } = new Object() // Tout est dépendant de cet objet
```

|       User       |
| :--------------: |
|  name (string)   |
| birthdate (date) |
| gender (Number)  |
|    sayHello()    |

Une instance est un objet créé avec une class.

```js
class User {
    constructor(name, birthdate, gender = 1){
        this.name = name; // "name" est le paramètre de la fonction et "this.name" est la propriété de la class, ils sont différent !!! Là on dit maPropriétéDeLinstance = monParamètreDeFonction
    }
    sayHello(){
        console.log(`Hello, my name is ${this.name} !`);
    }
}

let user1 = new User("Patate","15/07/89"); // Ici "this" remplace user1
let user2 = new User("Toto","15/07/89", 2);

user2.sayHello(); // Affiche dans la console "Hello, my name is Toto !"
```

