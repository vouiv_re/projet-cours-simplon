# TypeScript

* Le TypeScript est au JS ce que le SASS est au CSS.
* On peut faire du JS avec du TS
* Quasi retro-compatible
* Le TS ajoute du typage **statique** des variables et fonctions
* Il fait de "meilleure" class JS (prop ou methode : pulique, private)

> Le type "any" est à éviter !
>
> Un objet typé peut être "null" !