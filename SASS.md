# Sass - Scss 

* [Wikipedia](https://fr.wikipedia.org/wiki/Sass_(langage))
* [Site officiel](https://sass-lang.com/)

> (Syntactically Awesome Stylesheets)

C'est un super set

Sass : Pas d'accolade, mais indentation !!!

Scss : L'inverse

Imbrication des règles CSS

& = nom du parent

$ = variable

Il y a des types, on peut faires des opérations, on creer des variable

```sass
$margin : 5px
$block : 400px

taille de block = $block - 2*$margin
```
